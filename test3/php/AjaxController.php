<?php
/**
 * This code give response to an AJAX request.
 *
 * @author      Cayetnao H. Osma <chernandez@elestadoweb.com>
 * @date        October 2016
 *
 * @todo
 */

// To guarantee this function as secure, we need to check if is an AJAX request. If not we return a 405 Method not allowed.
// Para garantizar esta función como segura, necesitamos comprobar que sea llamada con un AJAX. Si no es así devolveremos un 405 Méthod not allowed.

/* decide what the content should be up here .... */
// $content = get_content(); //generic function;

/* AJAX check  */
if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {

    $information = [
        ['id' => 1, 'name' => 'Anthony', 'lastname' => 'Perez', 'email' => 'aperez0@noaa.gov','gender' => 'Male', 'ip' => '107.249.108.240'],
        ['id' => 2, 'name' => 'Nicole', 'lastname' => 'Morrison', 'email' => 'nmorrison1@auda.org.au','gender' => 'Female', 'ip' => '72.153.141.251'],
        ['id' => 3, 'name' => 'Randy', 'lastname' => 'Wallace', 'email' => 'rwallace2@hud.gov','gender' => 'Male', 'ip' => '245.117.6.151'],
        ['id' => 4, 'name' => 'Betty', 'lastname' => 'Morgan', 'email' => 'bmorgan3@opera.com','gender' => 'Female', 'ip' => '207.164.64.211'],
        ['id' => 5, 'name' => 'Wayne', 'lastname' => 'Harper', 'email' => 'wharper4@businesswire.com','gender' => 'Male', 'ip' => '103.171.191.108'],
        ['id' => 6, 'name' => 'Ashley', 'lastname' => 'Alexander', 'email' => 'aalexander5@tmall.com','gender' => 'Female', 'ip' => '22.80.242.57'],
        ['id' => 7, 'name' => 'Lillian', 'lastname' => 'Thompson', 'email' => 'lthompson6@indiegogo.com','gender' => 'Female', 'ip' => '144.179.96.80'],
        ['id' => 8, 'name' => 'Chris', 'lastname' => 'Reed', 'email' => 'creed7@ftc.gov','gender' => 'Male', 'ip' => '163.186.171.54'],
        ['id' => 9, 'name' => 'Justin', 'lastname' => 'Payne', 'email' => 'jpayne8@mayoclinic.com','gender' => 'Male', 'ip' => '226.13.129.80'],
        ['id' => 10, 'name' => 'Joan', 'lastname' => 'Arnold', 'email' => 'jarnold9@sina.com'.cn,'gender' => 'Female', 'ip' => '255.121.71.135'],
        ['id' => 11, 'name' => 'Margaret', 'lastname' => 'Myers', 'email' => 'mmyersa@mozilla.org','gender' => 'Female', 'ip' => '191.63.49.101'],
        ['id' => 12, 'name' => 'John', 'lastname' => 'Austin', 'email' => 'jaustinb@hao123.com','gender' => 'Male', 'ip' => '145.79.55.121'],
        ['id' => 13, 'name' => 'Roger', 'lastname' => 'Smith', 'email' => 'rsmithc@qq.com','gender' => 'Male', 'ip' => '183.50.191.1'],
        ['id' => 14, 'name' => 'Rose', 'lastname' => 'Riley', 'email' => 'rrileyd@tmall.com','gender' => 'Female', 'ip' => '64.47.222.50'],
        ['id' => 15, 'name' => 'Angela', 'lastname' => 'Frazier', 'email' => 'afraziere@so-net'.ne.jp,'gender' => 'Female', 'ip' => '80.245.68.235'],
        ['id' => 16, 'name' => 'Christina', 'lastname' => 'Medina', 'email' => 'cmedinaf@wordpress.org','gender' => 'Female', 'ip' => '91.86.80.133'],
        ['id' => 17, 'name' => 'George', 'lastname' => 'Green', 'email' => 'ggreeng@prweb.com','gender' => 'Male', 'ip' => '18.158.181.173'],
        ['id' => 18, 'name' => 'Doris', 'lastname' => 'Carroll', 'email' => 'dcarrollh@google.com'.br,'gender' => 'Female', 'ip' => '10.179.29.69'],
        ['id' => 19, 'name' => 'Laura', 'lastname' => 'Jackson', 'email' => 'ljacksoni@lulu.com','gender' => 'Female', 'ip' => '254.52.49.130'],
        ['id' => 20, 'name' => 'Frances', 'lastname' => 'Johnston', 'email' => 'fjohnstonj@foxnews.com','gender' => 'Female', 'ip' => '149.154.30.151'],
    ];

    /* special ajax here */
    die (json_encode([
        'status' => 200,
        'data' => $information
    ]));
}

/* not ajax, do more.... */
die (http_response_code(405));

