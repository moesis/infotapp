<?php
/**
 * Login for TEST 4.
 *
 * @author      Cayetnao H. Osma <chernandez@elestadoweb.com>
 * @date        October 2016
 * @todo
 */

session_start();
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    // Check the variables are set and are not empty.
    if (setAndNotEmpty($_POST['user']) && setAndNotEmpty($_POST['password'])) {

        // try to clean the input from form.
        $user = htmlentities($_POST['user']);
        $password = htmlentities($_POST['password']);

        // Here we need to check against database or whatever other source.
        // In this case we will use admin@admin.com as user and 'Password' as password.
        if ($user == 'admin@admin.com' && $password = 'Password') {
            $_SESSION['user'] = $user;
            header('Location: /devtry/infotapp//test4/loginsuccess.php');
        } else {
            $_SESSION['flash'] = 'Usuario o password incorrectos';
            header('Location: /devtry/infotapp//test4/index.php');
        }
    } else {
        $_SESSION['flash'] = 'Usuario o password incorrectos';
        header('Location: /devtry/infotapp//test4/index.php');
    }
} else {
    $_SESSION['flash'] = 'Método incorrectos';
    header('Location: /devtry/infotapp//test4/index.php');
}

/**
 * Check if variable is set and is not empty.
 * Return true if it is set and is not empty. False otherwise.
 *
 * Comprueba que la variable existe y no está vacía.
 * Retorna True si existe y no está vacía. False en cualquier otro caso
 *
 * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
 *
 * @param   mixed $variable
 *
 * @return  bool
 *
 * @todo
 */
function setAndNotEmpty($variable) {
    return (isset($variable) && !empty($variable));
}

die();
