<?php
/**
 * Login for TEST 4.
 *
 * @author      Cayetnao H. Osma <chernandez@elestadoweb.com>
 * @date        October 2016
 * @todo
 */

session_start();
if (isset($_SERVER['user'])) {
    unset($_SERVER['user']);
}

header('Location: /devtry/infotapp//test4/index.php');
