
<?php
    session_start();
    if (isset($_SERVER['HTTP_REFERER']) && strstr($_SERVER['HTTP_REFERER'], 'test4/index.php') == 0) : ?>
    <!DOCTYPE html >
    <html lang="es">
    <head>
        <meta charset="UTF-8">
        <title> Test 4 </title>
        <link rel="stylesheet" href="assets/bootstrap-3.3.7-dist/css/bootstrap.min.css" type="text/css" media="screen"/>
        <link rel="stylesheet" href="assets/login.css" type="text/css" media="screen"/>
    </head>
    <body>
    <div class="container">
        <div class="card card-container">
            <img id="profile-img" class="profile-img-card" src="//ssl.gstatic.com/accounts/ui/avatar_2x.png"/>
            <p id="profile-name" class="profile-name-card"></p>
            <p><?php echo $_SESSION['user']; ?></p>
            <h3 class="text-center"> identificación correcta.</h3>
            <h3 class="text-center"> Login satisfactorio.</h3>
            <div class="text-center">
                <a href="php/logout.php" >Abandonar la sesión</a>
            </div>
        </div>
    </div>
    </body>
    </html>
    <?php else: ?>
        <?php header('Location: /devtry/infotapp//test4/index.php'); ?>
<?php endif ?>

