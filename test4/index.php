<?php session_start(); ?>

<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <title>Test 4</title>
    <link rel="stylesheet" href="assets/bootstrap-3.3.7-dist/css/bootstrap.min.css" type="text/css" media="screen"/>
    <link rel="stylesheet" href="assets/login.css" type="text/css" media="screen"/>
</head>
<body>
    <div class="container">
        <div class="card card-container">
            <img id="profile-img" class="profile-img-card" src="//ssl.gstatic.com/accounts/ui/avatar_2x.png"/>
            <?php if ($_SESSION['flash']): ?>
                <div class="alert alert-danger alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <?= $_SESSION['flash'] ?>
                </div>
                <?php unset($_SESSION['flash']); ?>
            <?php endif ?>
            <p id="profile-name" class="profile-name-card"></p>
            <form class="form-signin" name="login_form" action="php/login.php" method="POST">
                <span id="reauth-email" class="reauth-email"></span>
                <input type="email" name="user" id="inputEmail" class="form-control" placeholder="Correo electrónico" required autofocus>
                <input type="password" name="password" id="inputPassword" class="form-control" placeholder="Contraseña" required>
                <div id="remember" class="checkbox">
                    <label>
                        <input type="checkbox" value="remember-me"> Recuerdame </label>
                </div>
                <button class="btn btn-lg btn-primary btn-block btn-signin" type="submit">Entrar</button>
            </form>
            <a href="#" class="forgot-password">¿Olvidó la contraseña?</a>
        </div>
    </div>

    <script type="text/javascript" src="assets/jquery-2.2.4.min.js"></script>
    <script type="text/javascript" src="../test2/assets/bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>

    <script type="text/javascript">
        $(document).ready(function () {

        });
    </script>

</body>
</html>