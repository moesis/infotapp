INTRODUCCIÓN

En esta prueba se le van a solicitar diferentes tareas. Están tratadas de forma independiente por lo que puede llevar a cabo las que se sienta capacitado a realizar.

Al finalizar las pruebas, si lo estima necesario, cree un documento explicando cualquier incidencia que tuviese o información sobre como poder ejecutar las pruebas.

PRUEBA 1

Con el código facilitado en la carpeta logout modifique el fichero que se encuentra en foo/index.php para que funcione del mismo modo que puede observar dentro de http://infotapp.com/devtry. Es una simulación de logout que contiene un bug en el código.


PRUEBA 2

Esta prueba está pensada para completarla usando Bootstrap pero no es necesario.

- A continuación cree una pequeña web HTML 5 responsive. Puede ver el diseño base en la imagen "Basic html5 structure.png". No hace falta que incluya el texto.

- Modifique la estructura anterior para que en tamaños de pantalla pequeño se oculten las barras laterales y se vea el "Main" ocupando todo el ancho de pantalla. Puede ver un ejemplo en "Basic html5 no sidebar.png";


PRUEBA 3

Esta prueba está pensada para completarla usando jQuery pero no es necesario.

Dentro de un div html deberá mostar el contenido que reciba de una petición AJAX. La petición deberá enviar datos mediante JSON y recibir otro JSON.

- Cree un respositorio en una plataforma de control de versiones, preferiblemente github.

- Cree toda la estructura HTML que pueda necesitar.

- Realice una petición AJAX simulando una llamada al servidor.

- Recibirá un JSON con datos del servidor (los datos los elige usted mismo).

- Muestre los datos recibidos del servidor en un div de forma legible para un ser humano.

- Haga un commit por cada una de las subtareas anteriores de consiga realizar con éxito.

PRUEBA 4

Realice todo el código necesario para desarrollar un login y logout usando PHP. Tiene libertad para usar los plugins, librerías, etc. que estime oportuno. Se valorará la funcionalidad y limpieza del código por encima del diseño.

Use un control de versiones, como github, para subir los códigos.


FINALIZACIÓN

Para enviar los resultados en los casos donde no se use un control de versiones, guarde los códigos dentro de una carpeta con el nombre de la prueba y comprímala en un .zip.
